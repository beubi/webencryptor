using Microsoft.Extensions.DependencyInjection;
using Encryptor.Services;
using Microsoft.AspNetCore.Mvc.Razor;
using System;
using Encryptor.Utils;
using System.Globalization;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Builder;
using System.Collections.Generic;

namespace Encryptor
{
    public static class ServiceExtensions
    {
        private const string RSA_PUBLIC_KEY_ENV_NAME = "RSA_PUBLIC_KEY";
        private const string PGP_PUBLIC_KEY_ENV_NAME = "PGP_PUBLIC_KEY";
        private const string PREPEND_TAG_ENV_NAME = "PREPEND_TAG";
        private const string APPEND_TAG_ENV_NAME = "APPEND_TAG";

        public static IServiceCollection RegisterEncryptor(this IServiceCollection services)
        {
            services.AddTransient<IKeyProvider>(serviceProvider =>
            {
                Algorithm al = AlgorithmResolver.getAlgorithm();
                if(Algorithm.RSA == al)
                {
                    return new RsaKeyProvider(
                        Environment.GetEnvironmentVariable(ServiceExtensions.RSA_PUBLIC_KEY_ENV_NAME)
                    );
                }

                return new PgpKeyProvider(
                    Environment.GetEnvironmentVariable(ServiceExtensions.PGP_PUBLIC_KEY_ENV_NAME)
                );
            });

            services.AddTransient<IEncryptionService>(serviceProvider =>
            {
                Algorithm al = AlgorithmResolver.getAlgorithm();
                IKeyProvider provider = serviceProvider.GetService<IKeyProvider>();

                if(AlgorithmResolver.isRsa())
                {
                    return new RsaEncryptor(provider);
                }

                return new PgpEncryptor(provider);
            });

            services.AddTransient<IResultFormatter>(serviceProvider =>
            {
                return new TagFormatter(
                    Environment.GetEnvironmentVariable(ServiceExtensions.PREPEND_TAG_ENV_NAME),
                    Environment.GetEnvironmentVariable(ServiceExtensions.APPEND_TAG_ENV_NAME)
                );
            });

            return services;
        }

        public static IServiceCollection ConfigureCultureServices(this IServiceCollection services)
        {
            services.AddLocalization(options => options.ResourcesPath = "Resources");
            services.AddMvc()
                .AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix)
                .AddDataAnnotationsLocalization();


            services.Configure<RequestLocalizationOptions>(
                opts =>
                {
                    var supportedCultures = new List<CultureInfo>
                    {
                        new CultureInfo("en"),
                        new CultureInfo("pt")
                    };

                    opts.DefaultRequestCulture = new RequestCulture("en");
                    // Formatting numbers, dates, etc.
                    opts.SupportedCultures = supportedCultures;
                    // UI strings that we have localized.
                    opts.SupportedUICultures = supportedCultures;

                    opts.RequestCultureProviders = new List<IRequestCultureProvider>
                    {
                        new CookieRequestCultureProvider()
                    };
                }
            );
    
            return services;
        }
    }
}