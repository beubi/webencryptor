using System;
using System.ComponentModel.DataAnnotations;

namespace Encryptor.Models
{
    public class EncryptionSubject
    {
        [DataType(DataType.Text)]
        [Required(ErrorMessage = "requiredText")]
        public string PlainText { get; set; }
        
        [DataType(DataType.Text)]
        public string EncryptedText { get; set; }
    }
}