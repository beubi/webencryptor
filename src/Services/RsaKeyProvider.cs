using Encryptor.Models;
using System;

namespace Encryptor.Services
{
    public class RsaKeyProvider : IKeyProvider
    {
        private string publicKey;

        public RsaKeyProvider(string base64PublicKey) 
        { 
            publicKey = this.decodeFromBase64(base64PublicKey);
        }

        public string getPublic() 
        {
            return this.publicKey;
        } 
        private string decodeFromBase64(string encodedString) 
        {
            string result;
            try
            {
                byte[] b = Convert.FromBase64String(encodedString);
                result = System.Text.Encoding.UTF8.GetString(b);
            }
            catch (FormatException) 
            {
                result = encodedString;
            }

            return result;            
        }
    }
}
