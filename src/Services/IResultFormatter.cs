using Encryptor.Models;

namespace Encryptor.Services
{
    public interface IResultFormatter
    {
        void format(EncryptionSubject subject); 
    }
}
