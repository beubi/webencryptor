using Encryptor.Models;

namespace Encryptor.Services
{
    public interface IEncryptionService
    {
        void encrypt(EncryptionSubject subject); 
    }
}
