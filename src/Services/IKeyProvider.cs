using Encryptor.Models;

namespace Encryptor.Services
{
    public interface IKeyProvider
    {
        string getPublic(); 
    }
}
