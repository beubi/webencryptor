using Encryptor.Models;
using Encryptor.Utils;
using System;
using System.IO;

namespace Encryptor.Services
{
    public class PgpEncryptor : IEncryptionService
    {
        private IKeyProvider _keyProvider;

        public PgpEncryptor(IKeyProvider provider)
        {
            _keyProvider = provider;
        }
        public void encrypt(EncryptionSubject subject)
        {
            var pubKey = this._keyProvider.getPublic();
            var clearText = subject.PlainText;
            using (var stream = pubKey.Streamify())
            {
                var key = stream.ImportPublicKey();
                using (var clearStream = clearText.Streamify())
                using (var cryptoStream = new MemoryStream())
                {
                    clearStream.PgpEncrypt(cryptoStream, key);
                    cryptoStream.Position = 0;
                    //var cryptoString = cryptoStream.Stringify();
                    subject.EncryptedText = Convert.ToBase64String(cryptoStream.ToArray());
                }
            }
        }
    }
}
