using Encryptor.Models;
using Encryptor.Services.Exceptions;
using System.Security.Cryptography;
using System.Text;
using System;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Asn1.Pkcs;
using System.IO;

namespace Encryptor.Services
{
    public class RsaEncryptor : IEncryptionService
    {
        private readonly bool _useOAEP = true;

        private readonly double _keySize = 4096;

        private IKeyProvider _keyProvider;

        private int _maxMessageLength;

        public RsaEncryptor(IKeyProvider provider)
        {
            _keyProvider = provider;
            _maxMessageLength = this.getMaxMessageLength(this._useOAEP, this._keySize);
        }
        public void encrypt(EncryptionSubject subject)
        {
            PemReader pemReader = new PemReader(new StringReader(this._keyProvider.getPublic()));
            RsaKeyParameters publicKeyParameters = (RsaKeyParameters)pemReader.ReadObject();
            RSAParameters rsaParameters = DotNetUtilities.ToRSAParameters(publicKeyParameters);

            using (var rsa = new RSACryptoServiceProvider())
            {
                try
                {
                    rsa.ImportParameters(rsaParameters);
                    byte[] dataToEncrypt = Encoding.UTF8.GetBytes(subject.PlainText);

                    this.assertMessageTooLong(dataToEncrypt);

                    byte[] encryptedData = rsa.Encrypt(dataToEncrypt, true);
                    subject.EncryptedText = Convert.ToBase64String(encryptedData);
                }
                finally
                {
                    rsa.PersistKeyInCsp = false;
                }
            }
        }

        private void assertMessageTooLong(byte[] message)
        {
            if (message.Length >= this._maxMessageLength)
            {
                throw new MessageTooLongException();
            }
        }

        private int getMaxMessageLength(bool useOaepPadding, double keySize)
        {
            double x = useOaepPadding ? 7 : 37;

            return (int)(Math.Floor((keySize - 384) / 8) + x);
        }
    }
}
