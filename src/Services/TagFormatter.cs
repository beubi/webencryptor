using Encryptor.Models;

namespace Encryptor.Services
{
    public class TagFormatter : IResultFormatter
    {
        private string _prependTag;
        private string _appendTag;

        public TagFormatter(string prependTag, string appendTag)
        {
            _prependTag = prependTag;
            _appendTag = appendTag;
        }

        public void format(EncryptionSubject subject)
        {
            subject.EncryptedText = this._prependTag + subject.EncryptedText + this._appendTag;
        }
    }
}
