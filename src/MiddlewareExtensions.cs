using System;
using Microsoft.AspNetCore.Builder;
using System.Globalization;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.DependencyInjection;

namespace Encryptor
{
    public static class MiddlewareExtensions
    {
        private const string PATH_BASE_ENV_NAME = "PATH_BASE";
        public static IApplicationBuilder SetPathBase(this IApplicationBuilder builder)
        {
            builder.UsePathBase(Environment.GetEnvironmentVariable(MiddlewareExtensions.PATH_BASE_ENV_NAME));
            return builder;
        }

        public static IApplicationBuilder ConfigureRequestLocalization(this IApplicationBuilder builder)
        {
            var options = builder.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            builder.UseRequestLocalization(options.Value);
        
            return builder;
        }
    }
}