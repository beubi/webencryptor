﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Encryptor.Models;
using Encryptor.Services;
using Encryptor.Services.Exceptions;

namespace Encryptor.Controllers
{
    public class HomeController : Controller
    {
        private readonly IEncryptionService _encryptionService; 
        private readonly IResultFormatter _tagFormatterService; 

        public HomeController(IEncryptionService encryptionService, IResultFormatter tagFormatterService) 
        { 
            _encryptionService = encryptionService; 
            _tagFormatterService = tagFormatterService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View(new EncryptionSubject());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Index([Bind("PlainText")] EncryptionSubject subject)
        {
            if (ModelState.IsValid) {
                try 
                {
                    this._encryptionService.encrypt(subject);
                    this._tagFormatterService.format(subject);
                }
                catch (MessageTooLongException) {
                    ViewData["isMessageTooLong"] = true;
                }
            }
            return View(subject);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
