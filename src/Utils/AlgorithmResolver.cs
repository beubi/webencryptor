using System;
using System.Linq;
using System.Text;
using System.IO;

using Org.BouncyCastle.Bcpg;
using Org.BouncyCastle.Bcpg.OpenPgp;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Security;

namespace Encryptor.Utils
{
    public enum Algorithm { PGP=1, RSA=2 };
    
    public static class AlgorithmResolver
    {   
        private const string ACTIVE_ALGORITHM_ENV_NAME = "ALGORITHM";

        public static bool isRsa() {
            Algorithm algorithm = AlgorithmResolver.getFromEnv();

            if (Algorithm.RSA == algorithm) {
                return true;
            }

            return false;
        }

        public static bool isPgp() {
            Algorithm algorithm = AlgorithmResolver.getFromEnv();

            if (Algorithm.PGP == algorithm) {
                return true;
            }

            return false;
        }

        public static Algorithm getAlgorithm() 
        {
            Algorithm algorithm = AlgorithmResolver.getFromEnv();

            if (Algorithm.PGP == algorithm) {
                return Algorithm.PGP;
            }

            if (Algorithm.RSA == algorithm) {
                return Algorithm.RSA;
            }

            throw new Exception("Unknown Algorithm option!!");
        }

        private static Algorithm getFromEnv()
        {
            return (Algorithm)Int32.Parse(Environment.GetEnvironmentVariable(AlgorithmResolver.ACTIVE_ALGORITHM_ENV_NAME));
        }
    }
}