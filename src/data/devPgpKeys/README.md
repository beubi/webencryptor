### PGP development key specifications

- RSA signing algorithm
- RSA encryption algorithm
- 3072 bit key
- key does not expire
- Real name: `webencryptor`
- Email address: `contact@beubi.com`
- Comment: `webencryptor demo`


**Key USER-ID:**

**"webencryptor (webencryptor demo) <contact@beubi.com>"**

**Passaword: `pwd`**