# Encryption webapp
___

This application is open source software released under the MIT License, written in `C#` using `.NET Core 2.2` framework. 

It can preform string encryption using one of the following algorithms

- PGP (using a public of any standard size and AES256 symmetric cipher)
- RSA (using a PEM public key and OAEP padding)

## Working with RSA
___

#### How to generate an RSA PEM key

Generate 2048 bit Private key
```
openssl genpkey -algorithm RSA -out private_key.pem -pkeyopt rsa_keygen_bits:2048
```

Separate the public part from the Private key file
```
openssl rsa -in private_key.pem -out public_key.pem -outform PEM -pubout
```

#### Decrypting

Given a set of encrypted data in base64 format, the content can be decrypted with the private key using the command above.
```
echo "<base64String>" | openssl base64 -d | openssl rsautl -decrypt -inkey private_key.pem -oaep

```

## Working with PGP
___

#### How to generate a PGP key

Generate 2048 bit key
```
gpg --full-generate-key
```
If you don't want the key in your default keying folder use the `--homedir <folder>` option.

Export the private and public keys to separated files
```
gpg --armor --export <email_if_you_have_more_than_one_key>  > public.gpg

gpg --armor --export-secret-keys <email_if_you_have_more_than_one_key>> private.gpg
```
Don't forget to add the `--homedir` option if you initial generated the key with it.

#### Decrypting

First you need to import a private key suitable for decryption:

```
gpg --import private.gpg
```

Given a set of encrypted data in base64 format, the content can be decrypted with the private key using the command above.
```
echo "<base64String>" | openssl base64 -d -A | gpg -d

```
or for bulk operations:

```
echo "<base64String>" | openssl base64 -d -A | gpg --batch --passphrase <pass> -d
```


`--homedir` might be needed if you key was not imported to the default gnuPG folder.
___


## Setting up application public keys
___

The application will be expecting the public keys in the `RSA_PUBLIC_KEY` and `PGP_PUBLIC_KEY` environment variables as a base64 encoded string or plain tex value, works both ways.

To encode a public key as a base64 string you can run the following command:
```
openssl base64 -A -in <key_file>
```
Grab the output and set it as an environment variable in your system (as a docker ENV for example).

For development purposes there are RSA and PGP key pairs located in the `data/` folder, that can be used to test the applications result. These keys are already set in the application if you are running it locally, these are the default keys.

## Application's working algorithm
___
In order to work the you need to tell the application what is the algorithm the will be used, thi is configure in `ALGORITHM` environment variable, possible values are `1` for PGP and `2` for RSA.

By default this value is `1` (PGP).

## Application's base url path
___
It is possible to configure the application's url path by setting the `PATH_BASE` environment variable, providing a value to this variable will make the all the application requests, including assets, be served through `http://<your-domain>/<PATH_BASE>`. This may be useful for serving multiple applications under the same domain.
By default this value is `/encryptor`

## Encryption result enclosuring tag
___
It is possible to format the encryption result with prepend and and append tags, for that you need to set the `PREPEND_TAG` and/or `APPEND_TAG` environment variables.

As an example, by setting these variables to PREPEND_TAG="[start]" and APPEND_TAG="[END]" the encryption result would look like:

```
[START]LS0tLS1CRUdJTiBQVU...JMSUMgS0VZLS0tLS0KTU[END]
```

By default these variables are empty, meaning that no enclosuring tag is applied to the result.

## Running locally
___
To launch the application locally you can simply execute `dotnet run` and it will run the `Development` mode.
The server will be running on port 5000 for HTTP and 5001 for HTTPS

If you are using `vscode` you can use the tasks in the `data/vscode` folder to run the application from a docker container, there are two configured in the `tasks.json` file:

- build - executes the application build using the started container on the start task
- watch - run the application with the file watcher activated

There is also a debug configuration in the `launch.json` file, you can use this configuration to debug application.

To use these tasks you need to install `Visual Studio Code Remote Development Extension Pack`..


## Running in production
___
The application is bundled as a docker container which you can build with a straight `docker build` command.

To run this container you should provide the public key you want to use, otherwise the default on will be used.

```
docker run -it -p 5000:5000 -e RSA_PUBLIC_KEY=<base64String> dockerImage
```

You can pull the latest version of the application from the `beubi/webencryptor` dockerhub repository.