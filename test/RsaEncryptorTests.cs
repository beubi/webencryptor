using Moq;
using NUnit.Framework;
using System;
using Encryptor.Models;
using Encryptor.Services;
using Encryptor.Services.Exceptions;
using Tests.Helpers;

namespace Tests
{
    public class RsaEncryptorTests
    {
        IKeyProvider KeyProvider;

        [OneTimeSetUp]
        public void Init()
        {
            var mock = new Mock<IKeyProvider>();
            mock.Setup(x => x.getPublic())
                .Returns(
                    System.Text.Encoding.UTF8.GetString(
                        Convert.FromBase64String(
                            Environment.GetEnvironmentVariable("RSA_PUBLIC_KEY")
                        )
                    )
                );

            this.KeyProvider = mock.Object;
        }

        [OneTimeTearDown]
        public void Cleanup()
        { 
            this.KeyProvider = null;
        }

        [TestCase("Text")]
        public void Encrypt_WithValidKey_ReturnsValidResult(string Text)
        {
            IEncryptionService Enc = this.buildRsaEncryptor();
            EncryptionSubject Payload = new EncryptionSubject();
            Payload.PlainText = Text;
            Enc.encrypt(Payload);

            TestHelpers.assertRsaEncryptionResult(Payload.EncryptedText, Text);
        }

        [TestCase("Text")]
        public void Encrypt_OnSuccess_ReturnsBase64Value(string Text)
        {
            IEncryptionService Enc = this.buildRsaEncryptor();
            EncryptionSubject Payload = new EncryptionSubject();
            Payload.PlainText = Text;
            Enc.encrypt(Payload);

            Assert.AreEqual(
                Payload.EncryptedText, 
                Convert.ToBase64String(Convert.FromBase64String(Payload.EncryptedText))
            );
        }

        [TestCase("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.")]
        public void Encrypt_PayloadTooBig_ThrowsException(string Text)
        {
            IEncryptionService enc = this.buildRsaEncryptor();
            EncryptionSubject payload = new EncryptionSubject();
            payload.PlainText = Text;
    
            Assert.Throws<MessageTooLongException>(() => enc.encrypt(payload));
        }

        private IEncryptionService buildRsaEncryptor()
        {
            return new RsaEncryptor(this.KeyProvider);
        }
    }
}