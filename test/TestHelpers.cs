using NUnit.Framework;
using System;
using System.IO;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Asn1.Pkcs;
using System.Security.Cryptography;
using System.Text;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Encodings;
using Encryptor.Utils;

namespace Tests.Helpers
{
    public static class TestHelpers
    {
        public static void assertRsaEncryptionResult(string result, string expected)
        {
            string privateKey = Environment.GetEnvironmentVariable("RSA_PRIVATE_KEY");

            byte[] resultRaw = Convert.FromBase64String(result);

            RsaPrivateCrtKeyParameters key;
            using (var reader = new StringReader(privateKey))
            {
                key = (RsaPrivateCrtKeyParameters)new PemReader(reader).ReadObject();
            }

            var eng = new OaepEncoding(new RsaEngine());
            eng.Init(false, key);
            var decrypted = Encoding.UTF8.GetString(eng.ProcessBlock(resultRaw, 0, resultRaw.Length));

            Assert.AreEqual(expected, decrypted);
        }

        public static void assertPgpEncryptionResult(string result, string expected)
        {
            var decryptedString = "";
            var privateKey = Environment.GetEnvironmentVariable("PGP_PRIVATE_KEY");
            var rawResult = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(result));

            using (var rawStream = rawResult.Streamify())
            {
                var decryptedStream = rawStream.PgpDecrypt(privateKey, Environment.GetEnvironmentVariable("PGP_PASSPHRASE"));
                decryptedString = decryptedStream.Stringify();
            }

            Assert.AreEqual(expected, decryptedString);
        }
    }
}